/**
 * Uses a byte array as a holder for a stream of bits and walks over these
 * bits one at a time.
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-11-24 18:02:25
 */
public class BitStreamIterator {
    private final byte[] bytes;
    private int byteLocation = 0;
    private int bitLocation = 0;

    public BitStreamIterator(byte[] bytes) {
        this.bytes = bytes;
    }

    public boolean next() {
        byte b = bytes[byteLocation];
        boolean bit = (b & (1 << bitLocation)) != 0; // if we and the byte with the bit location and we get a non-zero answer, then that bit was set
        bitLocation++;
        if (bitLocation == 8) {
            bitLocation = 0;
            byteLocation++;
        }
        return bit;
    }

    /**
     * @throws UnsupportedOperationException because removal is not supported.
     */
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove bits");
    }

    public boolean hasNext() {
        return byteLocation < bytes.length && bitLocation < 8;
    }

    public void skip(int offset) {
        // todo: there's a fancier way of doing this, but this will hav eto do for now
        while (offset-- > 0) {
            next();
        }
    }

    public static void main(String[] args) {
        BitStreamIterator bi = new BitStreamIterator(new byte[]{(byte) 15, (byte) 256});
        String s = "";
        while (bi.hasNext()) {
            s += (bi.next() ? "1" : "0");
        }
        System.out.println(s);
    }
/*
    public Iterator<Boolean> iterator() {
        return this;
    }
*/    
}
