import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-08-16 11:23:45
 */
public class FileReader {
    public static void main(String[] args) {
        long x = 0;
        long s = System.currentTimeMillis();
        FileInputStream fis = null;
        try {
            //
            fis = new FileInputStream("e:\\7100.0.090421-1700_x86fre_client_en-us_retail_ultimate-grc1culfrer_en_dvd.iso");
            //fis = new FileInputStream("C:\\Games\\Prototype\\movies.rcf");
            byte[] buf = new byte[8192];
            int c;
            while ((c = fis.read(buf)) != -1) {
                x += c;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        final long t = System.currentTimeMillis() - s;
        System.out.println("Took " + t + " milliseconds to read " + x + " bytes: " + ((double)x / (double)t) + " bytes per second");
    }
}
