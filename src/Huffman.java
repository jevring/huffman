import java.io.*;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-11-25 18:10:07
 */
public class Huffman {

    private class TreeNode {
        private char character = 0;
        private long frequency;
        private TreeNode left;
        private TreeNode right;

        private TreeNode() {
        }

        private TreeNode(char character) {
            this.character = character;
        }
    }

    private static class HuffmanArchive {
        private final int offset;
        private final byte[] bytes;
        private final Map<Character, Long> frequencies;

        private HuffmanArchive(int offset, byte[] bytes, Map<Character, Long> frequencies) {
            this.offset = offset;
            this.bytes = bytes;
            this.frequencies = frequencies;
        }
    }

    public HuffmanArchive encode(String input) { // todo: later we should probably be able to encode byte arrays as well. maybe
        // first pass, create the frequency table
        // note: this might turn out to be a terribly inefficient way of counting the frequency, but it's fun
        HashMap<Character, Long> frequencies = new HashMap<Character, Long>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            Long f = frequencies.get(c);
            if (f == null) {
                f = 0L;
            }
            frequencies.put(c, f+1);
        }

        TreeNode huffmanTree = createHuffmanTree(frequencies);
        /**
         * Maps characters to their bitstrings.
         * c -> 001
         * a -> 0100
         *
         * etc.
         */
        Map<Character, String> huffmanMap = putTreeIntoMap(huffmanTree);

        // the simplest way is to go via a long string here. It might not be the most efficient, though
        // todo: when everything else is working (storing tree on disk and stuff), attempt to rewrite this to put things into a byte array to start with
        // The problem with that is that we don't know how big the array needs to be from the beginning,
        // meaning that we might waste A LOT of space byt doubling it like a hash map. Memory space that
        // is. We can shrink it down after we're done, but by then the damage is already done. It would
        // be nice with some sort of linked-list structure for this. Might be too hard, though.
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            output.append(huffmanMap.get(c));
        }

        final String bitstring = output.toString();
        int offset = (8 - (bitstring.length() % 8)) % 8; // do the extra % 8 at the end, in case bitstring.length() is divisible by 8
        return new HuffmanArchive(offset, bitStreamToBytes(bitstring), frequencies);
        // todo: the offset is calculated twice. that seems unnecessary. fix that

        // in fact, we don't have to store the tree. If we can store the frequencies, that's enough to reconstruct everything!

    }

    public String decode(HuffmanArchive archive) {
        BitStreamIterator bi = new BitStreamIterator(archive.bytes);
        bi.skip(archive.offset);
        StringBuilder output = new StringBuilder();
        TreeNode huffmanTree = createHuffmanTree(archive.frequencies);
        TreeNode currentNode = huffmanTree; // todo: create this via the frequencies

        while (bi.hasNext()) {
            if (bi.next()) {
                currentNode = currentNode.right;
            } else {
                currentNode = currentNode.left;
            }
            if (currentNode.character != 0) {
                // only leaf nodes have characters, so if there is a char here, we should take it
                output.append(currentNode.character);
                currentNode = huffmanTree; // remember to reset for the next character!
            }
        }
        return output.toString();
    }

    private TreeNode createHuffmanTree(Map<Character, Long> frequencies) {
        //long treeCreationState = System.currentTimeMillis();
        // insert the frequencies in a priority queue, keyed on the frequency
        // this queue will later be used to create the tree.
        PriorityQueue<TreeNode> queue = new PriorityQueue<TreeNode>(frequencies.size(), new Comparator<TreeNode>() {
            /**
             * Compare the characters according to their frequencies. Lowest frequencies first.
             */
            public int compare(TreeNode o1, TreeNode o2) {
                long f1 = o1.frequency;
                long f2 = o2.frequency;
                if (f1 < f2) {
                    return -1;
                } else if (f1 > f2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        for (Map.Entry<Character, Long> frequencyEntry : frequencies.entrySet()) {
            final TreeNode tn = new TreeNode(frequencyEntry.getKey());
            tn.frequency = frequencyEntry.getValue();
            queue.add(tn);
        }

        // create the tree
        for (int i = 1; i < frequencies.size(); i++) {
            TreeNode z = new TreeNode();
            z.left = queue.poll();
            z.right = queue.poll();
            z.frequency = z.left.frequency + z.right.frequency;
            queue.add(z);
        }

        // this is the root of the tree that we will use when we look up characters
        TreeNode huffmanTree = queue.poll();

        //System.out.println("Time taken to create tree: " + (System.currentTimeMillis() - treeCreationState) + " milliseconds");
        return huffmanTree;
    }

    private Map<Character, String> putTreeIntoMap(TreeNode huffmanTree) {
        // walk the tree and put the entries in a map, so that we can look them up faster.
        // this is probably not the 'correct' way of doing it, but I honestly don't know
        // how we could search through the tree in a swift manner to build the bit string
        // note: actually, I believe this IS the correct way. When generating, a map is used. When reading, the tree is used.
        Map<Character, String> huffmanMap = new HashMap<Character, String>();
        walk(huffmanTree, "", huffmanMap);

        /*
        enter a node
        remember the bit string that got you to this node. probably by passing it in to the method
        if the node has a character, add that character to the huffman map, along with the bit string for this node
         */
        return huffmanMap;
    }

    private void walk(TreeNode node, String bits, Map<Character, String> huffmanMap) {
        if (node == null) {
            return;
        }
        //System.out.println("entering node " + node.character + " at depth " + bits.length() + " with bits " + bits);
        huffmanMap.put(node.character, bits);
        walk(node.left, bits + "0", huffmanMap);
        walk(node.right, bits + "1", huffmanMap);
    }

    private byte[] bitStreamToBytes(String bitstring) {
        byte[] output = new byte[((bitstring.length() + 7) / 8)];
        byte b = 0;
        int outputLocation = 0;
/* this is only good if we know that the bit string is a multiple of 8
        for (int i = 0; i < bitstring.length(); i++) {
            char c = bitstring.charAt(i);
            if (i % 8 == 0) {
                if (i > 0) {
                    output[outputLocation++] = b;
                }
                b = 0;
            }
            if (c == '1') {
                // flip this bit to 1
                //    00000000 (b)
                // OR 00000100 (1 << (2 % 8)), bit at index 2
                // -----------
                //    00000100 (b)
                b |= (byte) (1 << (i % 8));
            }
        }
*/
        // this is needed in case the length of the string is not a multiple of 8
        int offset = (8 - (bitstring.length() % 8)) % 8; // do the extra % 8 at the end, in case bitstring.length() is divisible by 8
        int bitIndex = offset;
        for (int i = 0; i < bitstring.length(); i++) {
            char c = bitstring.charAt(i);
            if (c == '1') {
                // flip this bit to 1
                //    00000000 (b)
                // OR 00000100 (1 << (2 % 8)), bit at index 2
                // -----------
                //    00000100 (b)
                b |= (byte) (1 << bitIndex);
            }
            if (++bitIndex == 8) {
                output[outputLocation++] = b;
                b = 0;
                bitIndex = 0;
            }
        }
        return output;
    }

    public static String readFile(String filename) throws IOException {
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(new FileInputStream(filename));
            StringBuilder sb = new StringBuilder();
            char[] cs = new char[1024 * 8];
            int c;
            while ((c = isr.read(cs)) != -1) {
                sb.append(cs, 0, c);
            }
            return sb.toString();
        } finally {
            if (isr != null) {
                isr.close();
            }
        }
    }

    public static void storeHuffmanArchive(HuffmanArchive archive, String filename) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            DataOutputStream dos = new DataOutputStream(fos);
            dos.writeInt(archive.offset);
            final String frequencies = archive.frequencies.toString();
            //dos.writeInt(frequencies.length());
            dos.writeUTF(frequencies);
            dos.writeInt(archive.bytes.length);
            dos.write(archive.bytes);
            fos.flush();
            fos.close();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                System.err.println("Could not close output stream when storing huffman archive");
            }
        }
    }

    public static HuffmanArchive readHuffmanArchive(String filename) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filename);
            DataInputStream dis = new DataInputStream(fis);
            int offset = dis.readInt();
            //int frequencyStringLength = dis.readInt();
            String frequencyString = dis.readUTF();
            int byteArrayLength = dis.readInt();
            byte[] bytes = new byte[byteArrayLength];
            dis.read(bytes);
            Map<Character, Long> frequencies = stringToMap(frequencyString);
            return new HuffmanArchive(offset, bytes, frequencies);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                System.err.println("Could not close input stream when reading huffman archive");
            }
        }
    }

    private static Map<Character, Long> stringToMap(String frequencyString) {
        Map<Character, Long> map = new HashMap<Character, Long>();
        // remove the enclosing gullwing brackets
        String[] tokens = frequencyString.substring(1, frequencyString.length() - 1).split(", ");
        char key;
        long value;
        for (String token : tokens) {
            String[] tokenParts;
            if (token.startsWith("=")) {
                token = token.trim();
                tokenParts = new String[]{"=", token.substring(2, token.length())};
            } else {
                tokenParts = token.split("=");
            }
            // the first token part will only have one char anyway
            try {
                //System.out.print("token _" + token + "_");
                //System.out.println(" parts: _" + tokenParts[0] + "_" + tokenParts[1] + "_");
                key = tokenParts[0].charAt(0);
                value = Long.parseLong(tokenParts[1].trim());
                map.put(key, value);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace(System.out);
                System.exit(1);
            }
        }
        return map;
    }

    public static void mainx(String[] args) {
        //String s = "{ a=123, b=45 }";
        //System.out.println("map: " + stringToMap(s));
        //System.exit(1);


        String filename;
        //filename = args[0];
        filename = "E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\bible\\bible.txt";
        //filename = "E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\ddict.short.txt"; // todo: this is just for testing purposes
        try {
            Huffman h = new Huffman();
            String input = readFile(filename);
            HuffmanArchive archive = h.encode(input);
            storeHuffmanArchive(archive, "c:\\bible.huffman");

            HuffmanArchive diskArchive = readHuffmanArchive("c:\\bible.huffman");
            String output = h.decode(archive);
            String diskOutput = h.decode(diskArchive);
            System.out.println("memory output matches disk read and decoded output: " + output.equals(diskOutput));
            System.out.println("input equals output: " + input.equals(output));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            if (args.length == 3) {
                Huffman h = new Huffman();
                if (args[0].equals("-expand")) {
                    HuffmanArchive archive = readHuffmanArchive(args[1]);
                    System.out.println("Encoded input is " + archive.bytes.length + " bytes long");
                    String output = h.decode(archive);
                    System.out.println("Original file size: " + output.length());
                    final double ratio = (double) archive.bytes.length / (double) output.length();
                    System.out.println("Compression ratio: [" + DecimalFormat.getNumberInstance().format(ratio) + ":1]");
                    writeFile(output, args[2]);
                } else if (args[0].equals("-compress")) {
                    String input = readFile(args[1]);
                    System.out.println("Original file size: " + input.length());
                    HuffmanArchive archive = h.encode(input);
                    System.out.println("Encoded input is " + archive.bytes.length + " bytes long");
                    final double ratio = (double) archive.bytes.length / (double) input.length();
                    System.out.println("Compression ratio: [1:" + DecimalFormat.getNumberInstance().format(ratio) + "]");
                    storeHuffmanArchive(archive, args[2]);
                }
            } else {
                System.out.println("usage: java -jar Huffman.jar {-compress, -expand} sourceFilename targetFilename");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeFile(String content, String filename) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            fos.write(content.getBytes());
            fos.flush();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                System.err.println("Could not close stream after saving content to file " + filename);
            }
        }

    }
}
