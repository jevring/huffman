import java.io.*;
import java.util.*;

/**
 * @author markus@jevring.net
 * @version $Id$
 * @since 2009-08-15 17:53:35
 */
public class HuffmanDev {
    private final HashMap<Character, Long> frequencies = new HashMap<Character, Long>();
    /**
     * Maps characters to their bitstrings.
     * c -> 001
     * a -> 0100
     *
     * etc.
     */
    private final HashMap<Character, String> huffmanMap = new HashMap<Character, String>();
    private TreeNode huffmanTree;

    public String encode(String input) {
        // first pass, create the frequency table
        // note: this might turn out to be a terribly inefficient way of counting the frequency, but it's fun
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            Long f = frequencies.get(c);
            if (f == null) {
                f = 0L;
            }
            frequencies.put(c, f+1);
        }
        //System.out.println("frequencies:");
        //System.out.println(frequencies);

        long treeCreationState = System.currentTimeMillis();
        // insert the frequencies in a priority queue, keyed on the frequency
        // this queue will later be used to create the tree.
        PriorityQueue<TreeNode> queue = new PriorityQueue<TreeNode>(frequencies.size(), new Comparator<TreeNode>() {
            /**
             * Compare the characters according to their frequencies. Lowest frequencies first.
             */
            public int compare(TreeNode o1, TreeNode o2) {
                long f1 = o1.frequency;
                long f2 = o2.frequency;
                if (f1 < f2) {
                    return -1;
                } else if (f1 > f2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        for (Map.Entry<Character, Long> frequencyEntry : frequencies.entrySet()) {
            final TreeNode tn = new TreeNode(frequencyEntry.getKey());
            tn.frequency = frequencyEntry.getValue();
            queue.add(tn);
        }
        
        // create the tree
        for (int i = 1; i < frequencies.size(); i++) {
            TreeNode z = new TreeNode();
            z.left = queue.poll();
            z.right = queue.poll();
            z.frequency = z.left.frequency + z.right.frequency;
            queue.add(z);
        }

        // this is the root of the tree that we will use when we look up characters
        huffmanTree = queue.poll();

        putTreeIntoMap();
        System.out.println("Time taken to create tree: " + (System.currentTimeMillis() - treeCreationState) + " milliseconds");

        //System.out.println(huffmanMap.size());
        //System.out.println("huffmap map: " + huffmanMap);

        // it would be nice if we could visualize the tree somehow, but that is, as always, quite hard

        // fine, we'll return a string one 1 and 0, just to get the ball rolling.
        // we clearly won't save any space doing this, but at least we'll know how it works
        // we could simply divide the size of the string by 8, which should give us an accurate size,
        // since the string "1" takes up 8 bits (1 byte) to represent.

        StringBuilder output = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            output.append(huffmanMap.get(c));
        }

        return output.toString();

        // todo: to output this as bits, create a byte array, and fill the byte with bits
        // since we might not have something divisible by 8, calculate the length, and pre-pad with that many ones or zeroes.
        // that way, we can read the padding first, before we dive in to the decoding, as opposed to checking each time if we're in the padding or not

        // pre-padding is a problem, though, if we don't know the size from the beginning.
        // of course, we will be writing to a byte array before writing to disk, so it's ok
        // 
        //http://stackoverflow.com/questions/93839/bit-manipulation-and-output-in-java



    }

    /**
     * Takes in a binary string, such as "1001001010101100101" and converts it to a byte
     * array with the same content. This byte array will be padded, and the first byte in
     * the byte array will indicate the size of the padding. The padding will be at the
     * BEGINNING of the file, not at the end.
     * @param input a binary string.
     * @return a byte array representing the binary string with actual bits.
     */
    private byte[] toByteArray(String input) {
        // todo: we must save the tree, too. Otherwise we're screwed. Thus, we might as well create a header that contains the tree and the padding offset


        // 1 is for the byte that indicates how much we should pad (represents an integer value between 0 and 7)
        // length+7 is so that the integer division doesn't drop one byte from our length
        // for instance, 9 / 8 is 1, but we need 2, so we add 7, making 16 / 8 = 2
        // this works even if we have 15 as a length. 15 + 7 = 22. 22/8 is still 2, which is what we want.

        // it might be inefficient to use a whole byte to indicate the padding length, but we have little choice, since
        // anything smaller would have to be inscribed in a byte anyway, for us to be able to get it out first
        byte[] output = new byte[1 + ((input.length() + 7) / 8)];
        byte b;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            /*
            to turn the string 11110000 into a byte, we do:
            - shift the current content one step to the left
            - OR tbe byte with the bit we want to add (how do we do this?)
                use OR to keep the previous values to what they were already
            - loop

            when we have done this 8 times, we add the byte to the array
             */
        }
        return output;
    }

    private void putTreeIntoMap() {
        // walk the tree and put the entries in a map, so that we can look them up faster.
        // this is probably not the 'correct' way of doing it, but I honestly don't know
        // how we could search through the tree in a swift manner to build the bit string

        walk(huffmanTree, "");

        /*
        enter a node
        remember the bit string that got you to this node. probably by passing it in to the method
        if the node has a character, add that character to the huffman map, along with the bit string for this node
         */
    }

    private void walk(TreeNode node, String bits) {
        if (node == null) {
            return;
        }
        //System.out.println("entering node " + node.character + " at depth " + bits.length() + " with bits " + bits);
        huffmanMap.put(node.character, bits);
        walk(node.left, bits + "0");
        walk(node.right, bits + "1");
    }

    public String decode(String input) {
        StringBuilder output = new StringBuilder();
        TreeNode currentNode = huffmanTree;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            // find the next possible letter based on the characters
            if (c == '1') {
                currentNode = currentNode.right;
            } else {
                currentNode = currentNode.left;
            }
            if (currentNode.character != 0) {
                // only leaf nodes have characters, so if there is a char here, we should take it
                output.append(currentNode.character);
                currentNode = huffmanTree; // remember to reset for the next character!
            }
        }
        return output.toString();
    }

    public String decode(byte[] input) {
        StringBuilder output = new StringBuilder();
        TreeNode currentNode = huffmanTree;

        BitStreamIterator bi = new BitStreamIterator(input);
        while (bi.hasNext()) {
            if (bi.next()) {
                currentNode = currentNode.right;
            } else {
                currentNode = currentNode.left;
            }
            if (currentNode.character != 0) {
                // only leaf nodes have characters, so if there is a char here, we should take it
                output.append(currentNode.character);
                currentNode = huffmanTree; // remember to reset for the next character!
            }
        }
        return output.toString();
    }

    private byte[] bitStringToByteArray(String input) {
        byte[] output = new byte[1 + ((input.length() + 7) / 8)];
        StringBuilder sb = new StringBuilder(8);
        int j = 0;
        for (int i = 0; i < input.length(); i++) {
            sb.append(input.charAt(i));
            if (i % 8 == 0) {
                output[j++] = (byte) Integer.parseInt(sb.toString(), 2);
                sb.delete(0, 8);
            }
        }
        //System.out.println("byte array: " + Arrays.toString(output));
        return output;
    }

    public HuffmanArchive compress(String input) {
        // todo: later, try to not use the intermediate string stage
        String bitString = encode(input);
        int offset = bitString.length() % 8;
        System.out.println("offset was " + offset);
        // actually, if we can insert 0-padding for the LSD, we might be able to pull it off. This means that, most likely, we have to pad inside the string, which sucks
        //bitString = bitString.substring(0, 8-offset) + zeroes(offset) + bitString.substring(8-offset);
        bitString = zeroes(offset) + bitString;
        System.out.println("bitstring: " + bitString);
        byte[] bytes = bitStringToByteArray(bitString); // todo: this doesn't work, because Integer.parseInt() can't take the offset in to account
        return new HuffmanArchive(bytes, offset);
    }

    private String zeroes(int howMany) {
        StringBuilder sb = new StringBuilder(howMany);
        while (howMany-- > 0) {
            sb.append('0');
        }
        return sb.toString();
    }

    public String expand(HuffmanArchive archive) {
        BitStreamIterator bi = new BitStreamIterator(archive.bytes);
        bi.skip(archive.offset);
        StringBuilder output = new StringBuilder();
        TreeNode currentNode = huffmanTree;

        while (bi.hasNext()) {
            if (bi.next()) {
                currentNode = currentNode.right;
            } else {
                currentNode = currentNode.left;
            }
            if (currentNode.character != 0) {
                // only leaf nodes have characters, so if there is a char here, we should take it
                output.append(currentNode.character);
                currentNode = huffmanTree; // remember to reset for the next character!
            }
        }
        return output.toString();
    }

    public byte[] toBytes(String bitstring) {
        byte[] output = new byte[((bitstring.length() + 7) / 8)];
        byte b = 0;
        int outputLocation = 0;
/* this is only good if we know that the bit string is a multiple of 8
        for (int i = 0; i < bitstring.length(); i++) {
            char c = bitstring.charAt(i);
            if (i % 8 == 0) {
                if (i > 0) {
                    output[outputLocation++] = b;
                }
                b = 0;
            }
            if (c == '1') {
                // flip this bit to 1
                //    00000000 (b)
                // OR 00000100 (1 << (2 % 8)), bit at index 2
                // -----------
                //    00000100 (b)
                b |= (byte) (1 << (i % 8));
            }
        }
*/
        // this is needed in case the length of the string is not a multiple of 8
        int offset = (8 - (bitstring.length() % 8)) % 8; // do the extra % 8 at the end, in case bitstring.length() is divisible by 8
        int bitIndex = offset;
        for (int i = 0; i < bitstring.length(); i++) {
            char c = bitstring.charAt(i);
            if (c == '1') {
                // flip this bit to 1
                //    00000000 (b)
                // OR 00000100 (1 << (2 % 8)), bit at index 2
                // -----------
                //    00000100 (b)
                b |= (byte) (1 << bitIndex);
            }
            if (++bitIndex == 8) {
                output[outputLocation++] = b;
                b = 0;
                bitIndex = 0;
            }
        }
        return output;
    }

    public static void main(String[] args) {
        try {
            //String input = readFile("E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\bible\\bible.txt");
            //String input = readFile("c:\\test.txt");
            String input = readFile("c:\\states.txt");
            // todo: test if we can convert the bitstring to a byte[] and then back again to get the same result

            HuffmanDev h = new HuffmanDev();
            String bitstring = h.encode(input);
            // this is where we turn the bitstring into a byte array, somehow
            byte[] bytes = h.toBytes(bitstring);
            System.out.println("1: " + bitstring);
            BitStreamIterator bi = new BitStreamIterator(bytes);
            // note: this offset would normally come from the method that turns the string into a byte array
            // however, in the future, we should be able to skip the bit string step and add the smaller bitstring
            // tokens straight to the array, foregoing that step
            int offset = (8 - (bitstring.length() % 8)) % 8; // do the extra % 8 at the end, in case bitstring.length() is divisible by 8
            bi.skip(offset);
            StringBuilder sb = new StringBuilder();
            while (bi.hasNext()) {
                sb.append(bi.next() ? "1" : "0");
            }
            System.out.println("2: " + sb.toString());

            System.out.println("bitstring = toString(toByteArray(bitstring)): " + bitstring.equals(sb.toString()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
/*
    public static void mainy(String[] args) {
        System.out.println(372 % 8);
        try {
            //String input = readFile("E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\bible\\bible.txt");
            String input = readFile("c:\\states.txt");

            // todo: test if we can convert the bitstring to a byte[] and then back again to get the same result





            //String input = readFile("E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\ddict.short.txt");
            System.out.println("Input is " + input.length() + " chars long");
            HuffmanDev h = new HuffmanDev();
            //System.out.println("5 zeroes: " + h.zeroes(5));
            final HuffmanArchive huffmanArchive = h.compress(input);
            System.out.println("Encoded input is " + huffmanArchive.bytes.length + " bytes long");
            System.out.println("Compression: " + ((double)huffmanArchive.bytes.length / (double)input.length()));
            String output = h.expand(huffmanArchive);
            //System.out.println("reencoded: (" + reencodedOutput.length() + " chars)\n" + reencodedOutput);
            boolean equal = input.equals(output);
            System.out.println("Original equals encode+decode?: " + equal);
            System.out.println(output);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void mainx(String[] args) {
        try {
            String input = readFile("E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\bible\\bible.txt");
            //String input = readFile("E:\\!data\\download\\!documents\\code\\Java\\retired\\ADOOP\\ddict.short.txt");
            System.out.println("Input is " + input.length() + " chars long");
            HuffmanDev h = new HuffmanDev();
            String encoded = h.encode(input);
            System.out.println("Encoded input is " + (encoded.length() / 8) + " bytes long");
            System.out.println("Compression: " + ((double)(encoded.length() / 8) / (double)input.length()));
            String reencodedOutput = h.decode(encoded);
            //System.out.println("reencoded: (" + reencodedOutput.length() + " chars)\n" + reencodedOutput);
            boolean equal = input.equals(reencodedOutput);
            System.out.println("Original equals encode+decode?: " + equal);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */

    public static String readFile(String filename) throws IOException {
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(new FileInputStream(filename));
            StringBuilder sb = new StringBuilder();
            char[] cs = new char[1024 * 8];
            int c;
            while ((c = isr.read(cs)) != -1) {
                sb.append(cs, 0, c);
            }
            return sb.toString();
        } finally {
            if (isr != null) {
                isr.close();
            }
        }
    }

    private class TreeNode {
        private char character = 0;
        private long frequency;
        private TreeNode left;
        private TreeNode right;

        private TreeNode() {
        }

        private TreeNode(char character) {
            this.character = character;
        }
    }

    private class HuffmanArchive {
        private byte[] bytes;
        private int offset;
        public HuffmanArchive(byte[] bytes, int offset) {
            this.bytes = bytes;
            this.offset = offset;
        }
    }
}
